# 泛域名使用https

# 有以下几个步骤(以阿里云为例)

1. 在aliyun生成AliyunDNSFullAccess的密钥对(搜索 ram)
2. 设置环境变量：
    ```
    export Ali_Key="XXXXXXXX"
    export Ali_Secret="XXXXXXXXXXXXXXX"
    ```
    ![图裂了](./imgs/cert_dnskey.png)
3. 生成泛域名证书，命令如下：
    ```
    acme.sh --issue --dns dns_ali -d *.9ihub.com
    ```
    ![图裂了](./imgs/cert_generate.png)
4. 安装生成的域名证书，命令如下：
    ```
    acme.sh --installcert -d *.9ihub.com --key-file /etc/nginx/ssl_common/*.9ihub.com.key --fullchain-file /etc/nginx/ssl_common/fullchain.cer --reloadcmd "systemctl force-reload nginx"
    ```
    ![图裂了](./imgs/cert_install.png)
    > 注意，ssl_common目录请提前创建好
5. 随便解析一个二级域名，这里以test为例，那么新的域名为test.9ihub.com
6. 设置新网站的web服务器（nginx）配置文件，在/etc/nginx/conf.d路径下，新增test.9ihub.com.conf配置文件，内容大概如下：
    ```
    server{
        # 输入普通地址时，跳转到https地址
        listen 80;
        server_name test.9ihub.com;
        return 301 https://test.9ihub.com$request_uri;
    }

    server{
        listen 443;  #监听的端口
        server_name test.9ihub.com; #监听的域名
            ssl on;
        ssl_certificate /etc/nginx/ssl_common/fullchain.cer;
        ssl_certificate_key /etc/nginx/ssl_common/*.9ihub.com.key;	
        location / {
            root /var/www/test.9ihub.com; #网站所在路径
            index index.html; #默认的首页文件
        }
    }

    ```
7. 参照之前的网站根目录，创建一个目录test.9ihub.com，位于/var/www目录下，随便放置一个index.html文件，尝试访问，如果能看到如下界面，恭喜你，你成功了！
    ![图裂了](./imgs/cert_result.png)